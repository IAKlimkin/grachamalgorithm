﻿namespace GrahamAlgorithm
{
    internal class Program
    {
        static void Main(string[] args)
        {
            StringBuilder timeline = new();
            timeline.Append("Count Time(ms)\n");
            StringBuilder iterations = new();
            iterations.Append("Count Iterations\n");
            for (int n = 200; n < 10000; n += 200)
            {
                List<Point> points = new List<Point>();
                StreamReader reader = new StreamReader($"C:\\СеместроваяАиСД (Алгоритм Грэхэма)\\PointsSets\\{n}.txt");
                while (reader.ReadLine() is { } line)
                {
                    var coordinates = line.Split(',');
                    points.Add(new Point() { X = int.Parse(coordinates[0]), Y = int.Parse(coordinates[1]) });
                }
            
                PointsList pointsList = new PointsList(points);
                timeline.Append($"{n} {pointsList.GetAlgorithmRunningTime()}\n");
                iterations.Append($"{n} {pointsList.GetIterationsCount()}\n");
                string path = $"C:\\СеместроваяАиСД (Алгоритм Грэхэма)\\Graphics\\Timeline.txt";
                using (StreamWriter sw = File.CreateText(path))
                {
                    sw.WriteLine(timeline.ToString().TrimEnd());
                }
                path = $"C:\\СеместроваяАиСД (Алгоритм Грэхэма)\\Graphics\\Iterations.txt";
                using (StreamWriter sw = File.CreateText(path))
                {
                    sw.WriteLine(iterations.ToString().TrimEnd());
                }
                Console.WriteLine($"Кол-во точек: {n}, \nВремя выполнения программы: {pointsList.GetAlgorithmRunningTime()}, \nКол-во итераций: {pointsList.GetIterationsCount()}");
            }


            PointsList pointsList = new PointsList(new List<Point?>()
            {
                new() { X = 11, Y = 6 },
                new() { X = 3, Y = 9 },
                new() { X = 6, Y = 6 },
                new() { X = 2, Y = 5 },
                new() { X = 10, Y = 4 },
                new() { X = 8, Y = 8 },
                new() { X = 10, Y = 10 },
                new() { X = 3, Y = 2 },
                new() { X = 6, Y = 10 },
                new() { X = 7, Y = 1 },
                new() { X = 1, Y = 7 },
                new() { X = 5, Y = 4 }
            });

            // foreach (var point in pointsList.Points)
            // {
            //     Console.WriteLine($"{point.X} - {point.Y}");
            // }
            //
            // Console.WriteLine("-----------------------------------------");

            foreach (var point in pointsList.ConvexHull.Get())
            {
                Console.WriteLine($"X:{point.X} Y:{point.Y}");
            }


            Console.WriteLine($"Время выполнения программы: {pointsList.GetAlgorithmRunningTime()}");
            Console.WriteLine($"Количество итераций: {pointsList.GetIterationsCount()}");
        }
    }
}