﻿namespace GrahamAlgorithm;

public class ConvexHull
{
    private readonly LinkedList<Point?> _list = new ();

    public ConvexHull(Point? startPoint, Point? firstPoint)
    {
        _list.AddLast(startPoint);
        _list.AddLast(firstPoint);
    }
    
    public void Push(Point? value)
    {
        _list.AddLast(value);
    }
    
    public void Pop()
    {
        _list.RemoveLast();
    }

    public Tuple<Point?, Point?> GetLastTwoPoints()
    {
        var p1 = _list.Last?.Value;
        Pop();
        var p2 = _list.Last?.Value;
        Push(p1);
        return new Tuple<Point?, Point?>(p1, p2);
    }
    
    public int Count() => _list.Count;
    public LinkedList<Point?> Get() => _list;
}