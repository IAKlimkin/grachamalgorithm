﻿using System.Diagnostics;

namespace GrahamAlgorithm;

public class PointsList
{
    public static int IterationsCounter = 0;
    public readonly List<Point?> Points;
    private static Point? _startPoint;
    public readonly ConvexHull ConvexHull;
    private readonly Stopwatch _stopwatch;

    public PointsList(List<Point?> points)
    {
        _stopwatch = new();
        _stopwatch.Start();
        Points = points;
        var startPointIndex = GetStartedPointIndex(points);
        _startPoint = points[startPointIndex];
        points.RemoveAt(startPointIndex);
        points = SortArray(points, 0, points.Count - 1);
        ConvexHull = new ConvexHull(_startPoint, points[0]);
        ConvexHull = CreateConvexHull(ConvexHull, points);
        _stopwatch.Stop();
    }

    private static Tuple<int, int> GetVector(Point? p1, Point? p2)
    {
        IterationsCounter++;
        return new Tuple<int, int>(p2.X - p1.X, p2.Y - p1.Y);
    }


    private static double GetVectorsComposition(Point? startPoint1, Point? startPoint2, Point? p1, Point? p2)
    {
        var vector1 = GetVector(startPoint1, p1);
        var vector2 = GetVector(startPoint2, p2);
        IterationsCounter++;
        return (vector1.Item1 * vector2.Item2) - (vector1.Item2 * vector2.Item1);
    }

    private static int Compare(Point? startPoint1, Point? startPoint2, Point? p1, Point? p2)
    {
        var vectorComposition = GetVectorsComposition(startPoint1, startPoint2, p1, p2);
        switch (vectorComposition)
        {
            case > 0:
                return -1;
            case < 0:
                return 1;
            default:
                return 0;
        }
    }

    private static List<Point?> SortArray(List<Point?> points, int leftIndex, int rightIndex)
    {
        var i = leftIndex;
        var j = rightIndex;
        var pivot = points[leftIndex];

        while (i <= j)
        {
            while (Compare(_startPoint, _startPoint, points[i], pivot) < 0)
            {
                IterationsCounter++;
                i++;
            }

            while (Compare(_startPoint, _startPoint, points[j], pivot) > 0)
            {
                IterationsCounter++;
                j--;
            }

            if (i <= j)
            {
                IterationsCounter++;
                (points[i], points[j]) = (points[j], points[i]);
                IterationsCounter++;
                i++;
                IterationsCounter++;
                j--;
            }
        }

        if (leftIndex < j)
            SortArray(points, leftIndex, j);

        if (i < rightIndex)
            SortArray(points, i, rightIndex);

        return points;
    }

    private static int GetStartedPointIndex(List<Point?> points)
    {
        int min = Int32.MaxValue;
        for (int i = 0; i < points.Count; i++)
        {
            if (points[i]!.X < min)
            {
                IterationsCounter++;
                min = i;
            }
        }

        return min;
    }


    private ConvexHull CreateConvexHull(ConvexHull convexHull, List<Point?> points)
    {
        var currentPointIndex = 1;

        while (currentPointIndex < points.Count)
        {
            IterationsCounter++;
            
            var lastPoints = convexHull.GetLastTwoPoints();
            var p1 = lastPoints.Item1;
            var p2 = lastPoints.Item2;
            var p3 = points[currentPointIndex];
            if (Compare(p2, p2, p1, p3) == 1) convexHull.Pop();
            convexHull.Push(p3);
            currentPointIndex++;
        }

        return convexHull;
    }

    public double GetAlgorithmRunningTime() => _stopwatch.ElapsedMilliseconds;
    public int GetIterationsCount() => IterationsCounter;
    public int Count() => Points.Count;
}